<?php
/**
 * Выполняет автозагрузку других модулей по указанным путям. Загрузка
 * модулей выполняется при помощи bootstrap файла, котоырй должен находится
 * в корне модуля и описывать модуль
 * @author B.Pavel <kiliman6aro@gmail.com>
 * @copyright 2017
 */

namespace ptmc\loader;


use yii\base\BootstrapInterface;
use yii\base\InvalidConfigException;

class Module extends \yii\base\Module implements BootstrapInterface
{
    const EVENT_BEFORE_LOADER = 'beforeLoader';


    const EVENT_AFTER_LOADER = 'afterLoader';

    /**
     * Массив алиасов или просто строка (alias) где
     * loader будет искать модули для загрузки
     * @var string | array
     */
    public $modulesAliases = '@app/modules';


    /**
     * Какие дирректории будут исключены из поиска.
     * Например могут быть модули, которые не нужно загружать
     * @var array
     */
    public $exclude = [];

    /**
     * В каждом модуле, который может быть загружен автоматически,
     * должен находится файл с описанием модуля, это имя файла, который
     * loader будет искать при парсинге.
     * @var string
     */
    public $bootstrapFileName = 'bootstrap.php';




    public function bootstrap($app)
    {
        $this->trigger(self::EVENT_BEFORE_LOADER);

        if(is_array($this->modulesAliases)){
            $this->findAll($this->modulesAliases);
        }else{
            $this->find($this->modulesAliases);
        }
        $this->trigger(self::EVENT_AFTER_LOADER);
    }

    /**
     * Находит модули (кталоги модулей) если передан сразу массив
     * путей (алиасов) где они могли бы находится
     * @param $aliases
     */
    protected function findAll($aliases)
    {
        foreach ($aliases as $alias){
            $this->find($alias);
        }
    }

    /**
     * Находит модули по указанному alias исключая системные дирректории ., .. а так же
     * перечисленные в параемтре exclude. Все найденные модули, пробует загрузить
     * передавая путь к модулю в метод load. @see Module::load
     * @param $alias
     * @throws InvalidConfigException
     */
    protected function find($alias)
    {
        $path = \Yii::getAlias($alias);
        if(!is_dir($path)){
           throw new InvalidConfigException(\Yii::t('app', 'Not found modules dirrectory {path}', ['path' => $path]));
        }
        $directories = array_diff(scandir($path), array_merge(['..', '.'], $this->exclude));
        foreach ($directories as $dir){
            $this->load($path.'/'.$dir);
        }
    }


    /**
     * Загружает модули при помощи bootstrap файлов. Если файл не найден
     * или он не соответствует структуре, то модуль будет пропущен. Если модуль
     * реализует интерфейс @see BootstrapInterface то будет вызван метод bootstrap
     * @param string $modulePath полный путь к модулю
     * @return bool|object
     */
    protected function load($modulePath)
    {
        $parent = \Yii::$app;

        $bootstrapFile = $modulePath.'/'.$this->bootstrapFileName;
        if(!file_exists($bootstrapFile)){
            \Yii::warning(\Yii::t('app', 'Bootstrap file for module not found. Path: {path}', ['path' => $bootstrapFile]));
            return false;
        }
        $config = require $bootstrapFile;

        if(!isset($config['id'])){
            \Yii::error(\Yii::t('app', 'ID module param is missing in file: {file}', ['file' => $bootstrapFile]));
            return false;
        }
        if(!isset($config['class'])){
            \Yii::error(\Yii::t('app', 'class name module param is missing in file: {file}', ['file' => $bootstrapFile]));
            return false;
        }

        if(isset($config['parent']) && \Yii::$app->hasModule($config['parent'])){
            $parent = \Yii::$app->getModule($config['parent']);
            unset($config['parent']);
        }
        $module = \Yii::createObject($config, [$config['id'], \Yii::$app]);
        $module->setInstance($module);
        $parent->setModule($config['id'], $module);
        if($module instanceof BootstrapInterface){
            $module->bootstrap(\Yii::$app);
        }
        \Yii::info(\Yii::t('app', 'Success autoload module {module} with bootstrap file: {file}', ['module' => $config['id'],'file' => $bootstrapFile]));
        return $module;
    }

}