Yii2 Loader modules
==================================
Yii2 loader for dynamic load modules and components

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist ptmc/yii2-loader "*"
```

or add

```
"ptmc/yii2-loader": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
bootstrap => ['loader', ...]

modules => [
    ....,
    'loader' => [
        'class' => 'ptmc\loader\Module'
    ]
]
```